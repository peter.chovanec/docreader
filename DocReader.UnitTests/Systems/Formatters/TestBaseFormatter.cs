﻿using DocReader.API.Dtos;
using DocReader.API.Formatters;
using DocReader.UnitTests.Fixtures;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Formatters;
using Moq;
using System.Text;


namespace DocReader.UnitTests.Systems.Services
{
    public class TestBaseFormatter
    {
        [Fact]
        public void Constructor_WithNullFormat_ThrowsArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => new BaseOutputFormatter(null));
        }

        [Fact]
        public async Task WriteResponseBodyAsync_NumberOfFormattedDocsFits()
        {
            // Arrange
            var docs = DocDtosFixture.GetTestDocs();

            var formatMock = new Mock<Func<DocDto, string>>();

            formatMock.Setup(x => x.Invoke(It.IsAny<DocDto>()))
                .Returns(string.Empty);

            var formatter = new BaseOutputFormatter(formatMock.Object);

            var httpContext = new DefaultHttpContext();
            var response = httpContext.Response;
            var outputContext = new OutputFormatterWriteContext(httpContext, (s, e) => new StreamWriter(s, e), typeof(List<DocDto>), docs);

            // Act
            await formatter.WriteResponseBodyAsync(outputContext, Encoding.UTF8);

            // Assert
            formatMock.Verify(x => x.Invoke(It.IsAny<DocDto>()), Times.Exactly(docs.Count));
        }
    }
}
