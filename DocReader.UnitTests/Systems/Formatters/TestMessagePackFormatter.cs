﻿using DocReader.API.Formatters;
using DocReader.UnitTests.Fixtures;
using System.Reflection;


namespace DocReader.UnitTests.Systems.Services
{
    public class TestMessagePackFormatter
    {

        [Fact]
        public void Format_ReturnsCorrectFormat()
        {
            // Arrange
            var docs = DocDtosFixture.GetTestDocs();
            var formatter = typeof(MessagePackOutputFormatter).GetMethod("Format", BindingFlags.NonPublic | BindingFlags.Static)!;

            // Act
            var results = docs.Select(doc => formatter.Invoke(null, new object[] { doc })).ToList();

            // Assert
            var expectedCsvs = DocMessagePackFixture.GetTestDocs();
            Assert.Equal(expectedCsvs, results);
        }
    }
}
