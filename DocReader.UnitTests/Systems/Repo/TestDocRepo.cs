﻿using DocReader.Data.Context;
using DocReader.Data.Models;
using DocReader.Data.Repos;
using DocReader.UnitTests.Fixtures;
using DocReader.UnitTests.Helpers;
using Microsoft.EntityFrameworkCore;


namespace DocReader.UnitTests.Systems.Repo
{
    public class TestDocRepo : IDisposable
    {
        private readonly DbContextOptions<RepoDbContext> _dbContextOptions;

        public TestDocRepo()
        {
            _dbContextOptions = MockDbContextHelper.InitDbContext();
        }

        public void Dispose()
        {
            using (var dbContext = new RepoDbContext(_dbContextOptions))
            {
                dbContext.Database.EnsureDeleted();
            }
        }

        [Fact]
        public void Constructor_WithNullDbContext_ThrowsArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => new DocRepo(null));
        }
       
        [Fact]
        public async Task GetAllDocumentsAsync_ReturnsAllDocuments()
        {
            using (var dbContext = new RepoDbContext(_dbContextOptions))
            {
                var docRepo = new DocRepo(dbContext);
                var cancellationToken = CancellationToken.None;

                // Act
                var result = await docRepo.GetAllDocumentsAsync(cancellationToken);

                // Assert
                Assert.Equal(DocModelsFixture.GetTestDocs().Count(), result.Count());
            }
        }




        [Fact]
        public async Task AddDocumentAsync_NewDocument_ReturnsTrue()
        {
            using (var dbContext = new RepoDbContext(_dbContextOptions))
            {
                var docRepo = new DocRepo(dbContext);
                var newDoc = new DocModel()
                {
                    Id = "#Notino_test",
                    Tags = new List<TagModel> {
                            new TagModel {
                                DocId = "#Notino_test",
                                Tag = "Tag1" },
                            new TagModel {
                                DocId = "#Notino_test",
                                Tag = "Tag2" },
                            new TagModel {
                                DocId = "#Notino_test",
                                Tag = "Tag3" }
                        },
                    Data = new List<DataModel> {
                            new DataModel {
                                DocId = "#Notino_test",
                                Key = "Key1",
                                Value = "Value1"},
                            new DataModel {
                                DocId = "#Notino_test",
                                Key = "Key2",
                                Value = "Value2"},
                            new DataModel {
                                DocId = "#Notino_test",
                                Key = "Key3",
                                Value = "Value3"}
                        }
                };
                var cancellationToken = CancellationToken.None;

                // Act
                var result = await docRepo.AddDocumentAsync(newDoc, cancellationToken);

                // Assert
                Assert.True(result);
            }
        }


        [Fact]
        public async Task AddDocumentAsync_ExistingDocument_ReturnsFalse()
        {
            using (var dbContext = new RepoDbContext(_dbContextOptions))
            {
                var docRepo = new DocRepo(dbContext);
                var existingDoc = dbContext.Documents.First();
                var cancellationToken = CancellationToken.None;

                // Act
                var result = await docRepo.AddDocumentAsync(existingDoc, cancellationToken);

                // Assert
                Assert.False(result);
            }
        }



        [Fact]
        public async Task GetOneDocumentAsync_ExistingDocumentId_ReturnsDocument()
        {
            using (var dbContext = new RepoDbContext(_dbContextOptions))
            {
                var docRepo = new DocRepo(dbContext);
                var existingDocId = "#Notino_reqs";
                var cancellationToken = CancellationToken.None;

                // Act
                var result = await docRepo.GetOneDocumentAsync(existingDocId, cancellationToken);

                // Assert
                Assert.NotNull(result);
                Assert.Equal(existingDocId, result.Id);
            }
        }


        [Fact]
        public async Task GetOneDocumentAsync_NonExistingDocumentId_ReturnsNull()
        {
            using (var dbContext = new RepoDbContext(_dbContextOptions))
            {
                var docRepo = new DocRepo(dbContext);
                var nonExistingDocId = "#Notino_none";
                var cancellationToken = CancellationToken.None;

                // Act
                var result = await docRepo.GetOneDocumentAsync(nonExistingDocId, cancellationToken);

                // Assert
                Assert.Null(result);
            }
        }

        [Fact]
        public async Task UpdateDocumentAsync_ExistingDocument_ReturnsTrue()
        {
            using (var dbContext = new RepoDbContext(_dbContextOptions))
            {
                var docRepo = new DocRepo(dbContext);
                var existingDoc = dbContext.Documents.First();
                var updatedDoc = new DocModel()
                {
                    Id = existingDoc.Id,
                    Tags = new List<TagModel> {
                            new TagModel {
                                DocId = "#Notino_test",
                                Tag = "Tag1" },
                            new TagModel {
                                DocId = "#Notino_test",
                                Tag = "Tag2" },
                            new TagModel {
                                DocId = "#Notino_test",
                                Tag = "Tag3" }
                        },
                    Data = new List<DataModel> {
                            new DataModel {
                                DocId = "#Notino_test",
                                Key = "Key1",
                                Value = "Value1"},
                            new DataModel {
                                DocId = "#Notino_test",
                                Key = "Key2",
                                Value = "Value2"},
                            new DataModel {
                                DocId = "#Notino_test",
                                Key = "Key3",
                                Value = "Value3"}
                        }
                };
                var cancellationToken = CancellationToken.None;

                // Act
                var result = await docRepo.UpdateDocumentAsync(updatedDoc, cancellationToken);

                // Assert
                Assert.True(result);
            }
        }



        [Fact]
        public async Task UpdateDocumentAsync_NonExistingDocument_ReturnsFalse()
        {
            using (var dbContext = new RepoDbContext(_dbContextOptions))
            {
                var docRepo = new DocRepo(dbContext);
                var updatedDoc = new DocModel()
                {
                    Id = "#Notino_none",
                    Tags = new List<TagModel> {
                            new TagModel {
                                DocId = "#Notino_none",
                                Tag = "Tag1" },
                            new TagModel {
                                DocId = "##Notino_none",
                                Tag = "Tag2" },
                            new TagModel {
                                DocId = "#Notino_none",
                                Tag = "Tag3" }
                        },
                    Data = new List<DataModel> {
                            new DataModel {
                                DocId = "#Notino_none",
                                Key = "Key1",
                                Value = "Value1"},
                            new DataModel {
                                DocId = "#Notino_none",
                                Key = "Key2",
                                Value = "Value2"},
                            new DataModel {
                                DocId = "#Notino_none",
                                Key = "Key3",
                                Value = "Value3"}
                        }
                };
                var cancellationToken = CancellationToken.None;

                // Act
                var result = await docRepo.UpdateDocumentAsync(updatedDoc, cancellationToken);

                // Assert
                Assert.False(result);
            }
        }
    }
}
