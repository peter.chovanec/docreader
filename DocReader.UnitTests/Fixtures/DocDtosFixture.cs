﻿using DocReader.API.Dtos;

namespace DocReader.UnitTests.Fixtures
{
    internal class DocDtosFixture
    {
        public static List<DocDto> GetTestDocs() =>
            new()
            {
                new DocDto()
                    {
                        Id = "#Notino_reqs",
                        Tags = new List<string> { ".net", "mssql", "docker" },
                        Data = new Dictionary<string, string> { 
                            { ".net", "senior" },
                            { "mssql", "advanced" },
                            { "docker", "junior" }
                        }
                    },
                    new DocDto()
                    {
                        Id = "#Notino_bonuses",
                        Tags = new List<string> { ".remote", "multisport", "cateferia" },
                        Data = new Dictionary<string, string> {
                            { ".remote", "possible" },
                            { "multisport", "yes" },
                            { "cateferia", "6000 CZK/year" }
                        }
                    },
                    new DocDto()
                    {
                        Id = "#Notino_contracts",
                        Tags = new List<string> {"hpp","ico" },
                        Data = new Dictionary<string, string> {
                            { "hpp", "yes" },
                            { "ico", "no" }
                        }
                    }
            };

    }
}
