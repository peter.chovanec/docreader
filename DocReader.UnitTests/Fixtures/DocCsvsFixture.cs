﻿namespace DocReader.UnitTests.Fixtures
{
    internal class DocCsvsFixture
    {
        public static List<string> GetTestDocs() =>
            new()
            {
                "\"#Notino_reqs\";\".net,mssql,docker\";\".net=senior,mssql=advanced,docker=junior\"",
                "\"#Notino_bonuses\";\".remote,multisport,cateferia\";\".remote=possible,multisport=yes,cateferia=6000 CZK/year\"",
                "\"#Notino_contracts\";\"hpp,ico\";\"hpp=yes,ico=no\""
            };
    }
}
