﻿using DocReader.Data.Context;
using DocReader.UnitTests.Fixtures;
using Microsoft.EntityFrameworkCore;

namespace DocReader.UnitTests.Helpers
{
    public static class MockDbContextHelper
    {
        public static DbContextOptions<RepoDbContext> CreateNewDbContextOptions()
        {
            var optionsBuilder = new DbContextOptionsBuilder<RepoDbContext>()
                .UseInMemoryDatabase(databaseName: "TestDatabase")
                .Options;
            return optionsBuilder;
        }

        public static DbContextOptions<RepoDbContext> InitDbContext()
        {
            var dbContextOptions = CreateNewDbContextOptions();

            using (var dbContext = new RepoDbContext(dbContextOptions))
            {
                dbContext.Documents.AddRange(DocModelsFixture.GetTestDocs());
                dbContext.SaveChanges();
            }

            return dbContextOptions;
        }
    }
}
