﻿using DocReader.Data.Models;
using Microsoft.EntityFrameworkCore;
using DocReader.Data.Context;

namespace DocReader.Data.Configs
{
    public class Db
    {
        public static void InitDb(RepoDbContext context, bool isProduction)
        {
            if (isProduction)
            {
                Console.WriteLine("--> Attempting to apply migrations ...");
                try
                {
                    context.Database.Migrate();
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"--> Could not run migrations: {ex.Message}");
                }
            }

            if (!context.Documents.Any())
            {
                Console.WriteLine("--> Seeding data");

                context.Documents.AddRange(
                    new DocModel()
                    {
                        Id = "#Notino_reqs",
                        Tags = new List<TagModel> {
                            new TagModel {
                                DocId = "#Notino_reqs",
                                Tag = ".net" },
                            new TagModel {
                                DocId = "#Notino_reqs",
                                Tag = "mssql" },
                            new TagModel {
                                DocId = "#Notino_reqs",
                                Tag = "docker" }
                        },
                        Data = new List<DataModel> {
                            new DataModel {
                                DocId = "#Notino_reqs",
                                Key = ".net",
                                Value = "senior"},
                            new DataModel {
                                DocId = "#Notino_reqs",
                                Key = "mssql",
                                Value = "advanced"},
                            new DataModel {
                                DocId = "#Notino_reqs",
                                Key = "docker",
                                Value = "junior"}
                        }
                    },
                    new DocModel()
                    {
                        Id = "#Notino_bonuses",
                        Tags = new List<TagModel> {
                            new TagModel {
                                DocId = "#Notino_bonuses",
                                Tag = ".remote" },
                            new TagModel {
                                DocId = "#Notino_bonuses",
                                Tag = "multisport" },
                            new TagModel {
                                DocId = "#Notino_bonuses",
                                Tag = "cateferia" }
                        },
                        Data = new List<DataModel> {
                            new DataModel {
                                DocId = "#Notino_bonuses",
                                Key = ".remote",
                                Value = "possible"},
                            new DataModel {
                                DocId = "#Notino_bonuses",
                                Key = "multisport",
                                Value = "yes"},
                            new DataModel {
                                DocId = "#Notino_bonuses",
                                Key = "cateferia",
                                Value = "6000 CZK/year"}
                        }
                    },
                    new DocModel()
                    {
                        Id = "#Notino_contracts",
                        Tags = new List<TagModel> {
                            new TagModel {
                                DocId = "#Notino_contracts",
                                Tag = "hpp" },
                            new TagModel {
                                DocId = "#Notino_contracts",
                                Tag = "ico" }
                        },
                        Data = new List<DataModel> {
                            new DataModel {
                                DocId = "#Notino_contracts",
                                Key = "hpp",
                                Value = "yes"},
                            new DataModel {
                                DocId = "#Notino_contracts",
                                Key = "ico",
                                Value = "no"}
                        }
                    }
                );
                context.SaveChanges();
            }
            else
            {
                Console.WriteLine("--> We already have a data");
            }

        }

    }
}
