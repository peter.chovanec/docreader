﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace DocReader.Data.Models
{
    public class TagModel
    {
        [Required, Key, Column(Order = 1)]
        public string Tag { get; set; } = String.Empty;

        [Required, Key, Column(Order = 2)]
        public string DocId { get; set; } = String.Empty;

        public DocModel Doc { get; set; }
    }
}
