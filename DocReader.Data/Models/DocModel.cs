﻿using System.ComponentModel.DataAnnotations;


namespace DocReader.Data.Models
{

    public class DocModel
    {
        [Key, Required]
        public string Id { get; set; } = String.Empty;

        [Required]
        public IEnumerable<TagModel> Tags { get; set; } = new List<TagModel>(); 

        [Required]
        public IEnumerable<DataModel> Data { get; set; } = new List<DataModel>(); 

    }
}

