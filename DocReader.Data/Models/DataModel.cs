﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace DocReader.Data.Models
{
    public class DataModel
    {
        [Required, Key, Column(Order = 1)]
        public string Key { get; set; } = String.Empty;

        [Required, Key, Column(Order = 2)]
        public string Value { get; set; } = String.Empty;

        [Required, Key, Column(Order = 3)]
        public string DocId { get; set; } = String.Empty;

        public DocModel Doc { get; set; }
    }
}
