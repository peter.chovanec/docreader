﻿using DocReader.Data.Context;
using DocReader.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using System.Text;
using X.PagedList;

namespace DocReader.Data.Repos
{
    public class DocRepoCache : IDocRepo
    {

        private readonly IMemoryCache _cache;
        private readonly IDocRepo _repo;

        public DocRepoCache(IMemoryCache cache, IDocRepo repo)
        {
            _cache = cache ?? throw new ArgumentNullException(nameof(cache));
            _repo = repo ?? throw new ArgumentNullException(nameof(repo));
        }


        public Task<bool> AddDocumentAsync(DocModel doc, CancellationToken cancellationToken)
            => _repo.AddDocumentAsync(doc, cancellationToken);

        public Task<bool> SaveChangesAsync(CancellationToken cancellationToken)
            => _repo.SaveChangesAsync(cancellationToken);

        public Task<bool> UpdateDocumentAsync(DocModel doc, CancellationToken cancellationToken)
            => _repo.UpdateDocumentAsync(doc, cancellationToken);


        public async Task<IEnumerable<DocModel>> GetAllDocumentsAsync(CancellationToken cancellationToken)
        {
            var cacheKey = $"{nameof(DocModel)}s";

            var cachedResponse = _cache.Get(cacheKey);

            if (cachedResponse is not null)
            {
                var res = JsonConvert.DeserializeObject<IEnumerable<DocModel>>(Encoding.Default.GetString((byte[])cachedResponse));
                if (res is not null)
                {
                    return res;
                }
            }

            var response = await _repo.GetAllDocumentsAsync(cancellationToken);
            var serResponse = JsonConvert.SerializeObject(response, Formatting.Indented,
                new JsonSerializerSettings
                {
                    PreserveReferencesHandling = PreserveReferencesHandling.Objects
                });

            _cache.Set(cacheKey, Encoding.Default.GetBytes(serResponse), new MemoryCacheEntryOptions()
            {
                AbsoluteExpiration = DateTime.Now.AddMinutes(60)
            });

            return response;
        }

        public Task<DocModel> GetOneDocumentAsync(string docId, CancellationToken cancellationToken)
        {
            var cacheKey = $"{nameof(DocModel)}s";

            var cachedResponse = _cache.Get(cacheKey);

            if (cachedResponse is not null)
            {
                var res = JsonConvert.DeserializeObject<IEnumerable<DocModel>>(Encoding.Default.GetString((byte[])cachedResponse));
                if (res is not null)
                {
                    var doc = res.FirstOrDefault(x => x.Id == docId);
                    if (doc is not null)
                        return Task.FromResult(doc);
                }
            }

            return _repo.GetOneDocumentAsync(docId, cancellationToken);
        }

        // Can be usefull one day when a huge amount of documents will be in database
        public Task<IEnumerable<DocModel>> GetPagedDocumentsAsync(int pageNumber, int pageSize, CancellationToken cancellationToken)
            => _repo.GetPagedDocumentsAsync(pageNumber, pageSize, cancellationToken);
    }
}
