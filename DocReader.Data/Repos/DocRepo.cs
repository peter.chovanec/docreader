﻿using DocReader.Data.Context;
using DocReader.Data.Models;
using Microsoft.EntityFrameworkCore;
using X.PagedList;

namespace DocReader.Data.Repos
{
    public class DocRepo : IDocRepo
    {

        private readonly RepoDbContext _dbContext;
        public DocRepo(RepoDbContext dbContext) => _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));


        public async Task<bool> AddDocumentAsync(DocModel doc, CancellationToken cancellationToken)
        {
            if (!await _dbContext.Documents.AnyAsync(x => x.Id == doc.Id, cancellationToken))
            {
                await _dbContext.Documents.AddAsync(doc, cancellationToken);
                return true;

            }

            return false;
        }

        public async Task<IEnumerable<DocModel>> GetAllDocumentsAsync(CancellationToken cancellationToken)
        {
            return await _dbContext.Documents.Include(d => d.Tags).Include(d => d.Data).ToListAsync(cancellationToken);
        }

        public async Task<DocModel> GetOneDocumentAsync(string docId, CancellationToken cancellationToken)
        {
            return (await _dbContext.Documents.Include(d => d.Tags).Include(d => d.Data).FirstOrDefaultAsync(x => x.Id == docId, cancellationToken))!;
        }

        // Can be usefull one day when a huge amount of documents will be in database
        public async Task<IEnumerable<DocModel>> GetPagedDocumentsAsync(int pageNumber, int pageSize, CancellationToken cancellationToken)
        {
            return await _dbContext.Documents.Include(d => d.Tags).Include(d => d.Data).AsEnumerable().ToPagedListAsync<DocModel>(pageNumber, pageSize, cancellationToken);
        }

        public async Task<bool> SaveChangesAsync(CancellationToken cancellationToken)
        {
            return (await _dbContext.SaveChangesAsync(cancellationToken) >= 0);
        }

        public async Task<bool> UpdateDocumentAsync(DocModel doc, CancellationToken cancellationToken)
        {
            DocModel? oldDoc = await _dbContext.Documents.Include(d => d.Tags).Include(d => d.Data).FirstOrDefaultAsync(x => x.Id == doc.Id, cancellationToken);
            if (oldDoc is not null)
            {
                oldDoc.Tags = doc.Tags;
                oldDoc.Data = doc.Data;
                return true;
            }

            return false;
        }



    }
}
