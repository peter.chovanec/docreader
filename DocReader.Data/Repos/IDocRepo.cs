﻿using DocReader.Data.Models;

namespace DocReader.Data.Repos
{
    public interface IDocRepo
    {
        public Task<IEnumerable<DocModel>> GetAllDocumentsAsync(CancellationToken cancellationToken);

        public Task<DocModel> GetOneDocumentAsync(string docId, CancellationToken cancellationToken);

        public Task<IEnumerable<DocModel>> GetPagedDocumentsAsync(int pageNumber, int pageSize, CancellationToken cancellationToken);

        public Task<bool> AddDocumentAsync(DocModel doc, CancellationToken cancellationToken);
        public Task<bool> UpdateDocumentAsync(DocModel doc, CancellationToken cancellationToken);

        public Task<bool> SaveChangesAsync(CancellationToken cancellationToken);

    }
}
