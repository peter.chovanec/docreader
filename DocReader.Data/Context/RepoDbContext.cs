﻿using DocReader.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
namespace DocReader.Data.Context
{
    public class RepoDbContext : DbContext
    {
        public RepoDbContext()
        {
        }

        public RepoDbContext(DbContextOptions<RepoDbContext> opt) : base(opt)
        {

        }

        public DbSet<DocModel> Documents { get; set; }
        public DbSet<TagModel> Tags { get; set; }
        public DbSet<DataModel> Data { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<DocModel>()
                .HasMany(p => p.Tags)
                .WithOne(p => p.Doc!)
                .HasForeignKey(p => p.DocId);

            modelBuilder.Entity<TagModel>()
                .HasKey(m => new { m.Tag, m.DocId });

            modelBuilder
                .Entity<TagModel>()
                .HasOne(p => p.Doc)
                .WithMany(p => p.Tags)
                .HasForeignKey(p => p.DocId);

            modelBuilder.Entity<DataModel>()
                .HasKey(m => new { m.Key, m.Value, m.DocId });

            modelBuilder.Entity<DataModel>()
                .HasOne(p => p.Doc)
                .WithMany(p => p.Data)
                .HasForeignKey(p => p.DocId);
        }


    }
}
