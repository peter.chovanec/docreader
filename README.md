# Doc Reader - Case Study for Notino
Production-ready ASP.NET Core service app that provides API for storage and retrive documents in different formats

## Tasks
1. The documents are send as a payload of POST request in JSON format and could be modified via PUT verb.

2. The service is able to return the stored documents in different format, such as XML, MessagePack, etc.

3. It must be easy to add support for new formats.

4. It must be easy to add different underlying storage, like cloud, HDD, InMemory, etc.

5. Assume that the load of this service will be very high (mostly reading).

6. Demonstrate ability to write unit tests.

7. The document has mandatory field id, tags and data as shown bellow. The schema of the data field can be arbitrary.

```
POST http://localhost:5000/documents
Content-Type: application/json; charset=UTF-8
{
 "id": "some-unique-identifier1",
 "tags": ["important", ".net"]
 "data": {
   "some": "data",
   "optional": "fields"
  }
}
GET http://localhost:5000/documents/some-unique-identifier1
Accept: application/xml
```

## About

This repo contains: 
- REST API service providing all required functions
- Swagger documentation of all endpoints
- Unit tests covering functionality
- Caching of loaded data from database
- Docker compose with api and db (migration of database and seed of the sample data included)

## Solution

### Major facts: 
- Development version based on InMemory database, Production version based on MS SQL
- Project has three layers : API, Application Layer and Repository

### Used technologies
- .NET 6
- Entity Framework

### Used tools
- Git
- Docker

### Used nugets
- MediatR
- Automapper
- Scrutor
- MessagePack
- Microsoft.EntityFrameworkCore.InMemory
- Microsoft.EntityFrameworkCore.SqlServer
- Newtonsoft.Json
- X.PagedList
- Moq

