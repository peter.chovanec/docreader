﻿using DocReader.Data.Models;
using MediatR;


namespace DocReader.AppLayer.CQRS.Queries
{
    public record GetDocumentByIdQuery(string id) : IRequest<DocModel>;
}
