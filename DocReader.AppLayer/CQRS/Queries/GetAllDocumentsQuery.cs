﻿using DocReader.Data.Models;
using MediatR;

namespace DocReader.AppLayer.CQRS.Queries
{
    public record GetAllDocumentsQuery() : IRequest<IEnumerable<DocModel>>;

}
