﻿using DocReader.AppLayer.CQRS.Commands;
using DocReader.Data.Repos;
using MediatR;

namespace DocReader.AppLayer.CQRS.CommandsHandlers
{
    public class UpdateDocumentHandler : IRequestHandler<UpdateDocumentCommand, bool>
    {
        private readonly IDocRepo _repo;
        public UpdateDocumentHandler(IDocRepo repo)
        {
            _repo = repo ?? throw new ArgumentNullException(nameof(repo));
        }


        public Task<bool> Handle(UpdateDocumentCommand request, CancellationToken cancellationToken)
        {
            var result = _repo.UpdateDocumentAsync(request.document, cancellationToken);
            _repo.SaveChangesAsync(cancellationToken);
            return result;
        }
    }
}
