﻿using DocReader.AppLayer.CQRS.Commands;
using DocReader.Data.Repos;
using MediatR;

namespace DocReader.AppLayer.CQRS.CommandsHandlers
{
    public class AddDocumentHandler : IRequestHandler<AddDocumentCommand, bool> 
    {
        private readonly IDocRepo _repo;
        public AddDocumentHandler(IDocRepo repo)
        {
            _repo = repo ?? throw new ArgumentNullException(nameof(repo));
        }

        public Task<bool> Handle(AddDocumentCommand request, CancellationToken cancellationToken)
        {
            var result =  _repo.AddDocumentAsync(request.document, cancellationToken);
            _repo.SaveChangesAsync(cancellationToken);
            return result;
        }
    }
}
