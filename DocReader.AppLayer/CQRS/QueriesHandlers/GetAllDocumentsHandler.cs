﻿using DocReader.AppLayer.CQRS.Queries;
using DocReader.Data.Models;
using DocReader.Data.Repos;
using MediatR;

namespace DocReader.AppLayer.CQRS.QueriesHandlers
{
    public class GetAllDocumentsHandler : IRequestHandler<GetAllDocumentsQuery, IEnumerable<DocModel>>
    {
        private readonly IDocRepo _repo;

        public GetAllDocumentsHandler(IDocRepo repo)
        {
            _repo = repo ?? throw new ArgumentNullException(nameof(repo));
        }

        public Task<IEnumerable<DocModel>> Handle(GetAllDocumentsQuery request, CancellationToken cancellationToken) =>  _repo.GetAllDocumentsAsync(cancellationToken);
    }

}
