﻿using DocReader.AppLayer.CQRS.Queries;
using DocReader.Data.Models;
using DocReader.Data.Repos;
using MediatR;

namespace DocReader.AppLayer.CQRS.QueriesHandlers
{
    public class GetDocumentByIdHandler : IRequestHandler<GetDocumentByIdQuery, DocModel>
    {
        private readonly IDocRepo _repo;

        public GetDocumentByIdHandler(IDocRepo repo)
        {
            _repo = repo ?? throw new ArgumentNullException(nameof(repo));
        }

        public  async Task<DocModel> Handle(GetDocumentByIdQuery request, CancellationToken cancellationToken) =>  await _repo.GetOneDocumentAsync(request.id, cancellationToken);

    }
}
