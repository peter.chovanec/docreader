﻿using DocReader.Data.Models;
using MediatR;


namespace DocReader.AppLayer.CQRS.Commands
{
    public record AddDocumentCommand(DocModel document) : IRequest<bool>;
}
