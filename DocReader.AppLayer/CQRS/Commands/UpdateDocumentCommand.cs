﻿using DocReader.Data.Models;
using MediatR;


namespace DocReader.AppLayer.CQRS.Commands
{
    public record UpdateDocumentCommand(DocModel document) : IRequest<bool>;
}
