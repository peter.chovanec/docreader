﻿using AutoMapper;
using DocReader.API.Dtos;
using DocReader.API.Formatters.Dtos;
using DocReader.Data.Models;

namespace DocReader.API.Profiles
{
    public class DocsProfile : Profile
    {
        public DocsProfile()
        {
            CreateMap<DocModel, DocDto>()
                .ForMember(dest => dest.Tags, opt => opt.MapFrom(src => src.Tags.Select(t => t.Tag).ToList()))
                .ForMember(dest => dest.Data, opt => opt.MapFrom(src => src.Data.ToDictionary(d => d.Key, d => d.Value)));

            CreateMap<DocDto, DocModel>()
                .ForMember(dest => dest.Tags, opt => opt.MapFrom(src => src.Tags.Select(t => new TagModel { Tag = t, DocId = src.Id }).ToList()))
                .ForMember(dest => dest.Data, opt => opt.MapFrom(src => src.Data.Select(kv => new DataModel { Key = kv.Key, Value = kv.Value, DocId = src.Id }).ToList()));

            CreateMap<string, DocModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src));

            string id = "";
            CreateMap<UpdateDocDto, DocModel>()
                .BeforeMap((src, dest) => id = dest.Id)
                .ForMember(dest => dest.Tags, opt => opt.MapFrom(src => src.Tags.Select(t => new TagModel { Tag = t, DocId = id }).ToList()))
                .ForMember(dest => dest.Data, opt => opt.MapFrom(src => src.Data.Select(kv => new DataModel { Key = kv.Key, Value = kv.Value, DocId = id }).ToList()));

            CreateMap<DocDto, MessagePackDocDto>();
        }
    }
}
