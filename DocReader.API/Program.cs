using DocReader.Data.Repos;
using Microsoft.EntityFrameworkCore;
using System;
using AutoMapper;
using DocReader.Data.Context;
using DocReader.Data.Configs;
using DocReader.AppLayer;
using DocReader.API.Formatters;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();


builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

if (builder.Environment.IsDevelopment())
{
    builder.Services.AddDbContext<RepoDbContext>(opt => opt.UseInMemoryDatabase("InMem"));
}
else
{
    builder.Services.AddDbContext<RepoDbContext>(opt => opt.UseSqlServer(builder.Configuration.GetConnectionString("DocReaderConnection")));
}

builder.Services.AddMediatR(cfg => cfg.RegisterServicesFromAssembly(typeof(AppLayerEntryPoint).Assembly));
builder.Services.AddScoped<IDocRepo, DocRepo>();

// Cache for more efficient query processing over persistent data storage
// We do not have to use it in development, since InMemory database is used then
if (!builder.Environment.IsDevelopment())
{
    builder.Services.AddMemoryCache();
    builder.Services.Decorate<IDocRepo, DocRepoCache>();
}


builder.Services.AddControllers(options =>
{
    options.RespectBrowserAcceptHeader = true;
})
.AddXmlSerializerFormatters()
.AddXmlDataContractSerializerFormatters()
.AddMvcOptions(options => {
    options.OutputFormatters.Add(new CsvOutputFormatter());
    options.OutputFormatters.Add(new MessagePackOutputFormatter());
});

var app = builder.Build();

// Configure the HTTP request pipeline.
//if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

//app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

using (var serviceScope = app.Services.CreateScope())
{
    Db.InitDb(serviceScope.ServiceProvider.GetService<RepoDbContext>(), builder.Environment.IsProduction());
}

app.Run();
