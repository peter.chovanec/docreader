﻿using System.ComponentModel.DataAnnotations;

namespace DocReader.API.Dtos
{
    public class UpdateDocDto
    {
        [Required]
        public IEnumerable<string> Tags { get; set; } = new List<string>();

        [Required]
        public IDictionary<string, string> Data { get; set; } = new Dictionary<string, string>();
    }
}
