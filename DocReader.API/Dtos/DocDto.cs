﻿using DocReader.Data.Models;
using System.ComponentModel.DataAnnotations;

namespace DocReader.API.Dtos
{
    public class DocDto
    {
        [Required]
        public string Id { get; set; } = string.Empty;

        [Required]
        public IEnumerable<string> Tags { get; set; } = new List<string>();

        [Required]
        public IDictionary<string, string> Data { get; set; } = new Dictionary<string, string>();
    }
}
