using AutoMapper;
using DocReader.API.Dtos;
using DocReader.AppLayer.CQRS.Commands;
using DocReader.AppLayer.CQRS.Queries;
using DocReader.Data.Models;
using MediatR;
using Microsoft.AspNetCore.Mvc;


namespace DocReader.API.Controllers
{
    [ApiController]
    [Route("documents")]
    public class DocReaderController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IMediator _mediator;


        public DocReaderController(IMapper mapper,  IMediator mediator)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper)); 
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator)); 
        }

        /// <summary>
        /// Retrieves all documents.
        /// </summary>
        /// <param name="cancellationToken">Cancellation token.</param>
        /// <returns>List of documents.</returns>
        [HttpGet(Name = nameof(GetAllDocuments))]
        [ProducesResponseType(typeof(DocDto), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetAllDocuments(CancellationToken cancellationToken)
        {
            var docs = await _mediator.Send(new GetAllDocumentsQuery(), cancellationToken);
            return docs is null ? NotFound() : Ok(_mapper.Map<List<DocDto>>(docs));
        }

        /// <summary>
        /// Retrieves a specific document by ID.
        /// </summary>
        /// <param name="id">The ID of the document.</param>
        /// <param name="cancellationToken">Cancellation token.</param>
        /// <returns>The document with the specified ID.</returns>
        [HttpGet("{id}", Name = nameof(GetOneDocument))]
        [ProducesResponseType(typeof(DocDto), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetOneDocument(string id, CancellationToken cancellationToken)
        {
            var doc = await _mediator.Send(new GetDocumentByIdQuery(id), cancellationToken);
            return doc is null ? NotFound() : Ok(_mapper.Map<DocDto>(doc));
        }

        /// <summary>
        /// Adds a new document.
        /// </summary>
        /// <param name="doc">The document to be added.</param>
        /// <param name="cancellationToken">Cancellation token.</param>
        /// <returns>Response for the document creation.</returns>
        [HttpPost(Name = nameof(AddDocument))]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> AddDocument(DocDto doc, CancellationToken cancellationToken)
        {
            bool added = await _mediator.Send(new AddDocumentCommand(_mapper.Map<DocModel>(doc)), cancellationToken);
            return (added) ? CreatedAtAction(nameof(GetOneDocument), new { id = doc.Id }, doc) : BadRequest();
        }


        /// <summary>
        /// Updates an existing document by ID.
        /// </summary>
        /// <param name="id">The ID of the document to be updated.</param>
        /// <param name="doc">The updated document data.</param>
        /// <param name="cancellationToken">Cancellation token.</param>
        /// <returns>Response for the document update.</returns>
        [HttpPut("{id}", Name = nameof(UpdateDocument))]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> UpdateDocument(string id, UpdateDocDto doc, CancellationToken cancellationToken)
        {
            var docModel = _mapper.Map<DocModel>(id);
            docModel = _mapper.Map(doc, docModel);

            bool updated = await _mediator.Send(new UpdateDocumentCommand(docModel), cancellationToken);

            return (updated) ? Ok(doc) : NotFound();

        }

    }
}