﻿using MessagePack;
using System.ComponentModel.DataAnnotations;

namespace DocReader.API.Formatters.Dtos
{
    [MessagePackObject]
    public class MessagePackDocDto
    {
        [Required]
        [MessagePack.Key(0)]
        public string Id { get; set; }

        [Required]
        [MessagePack.Key(1)]
        public IEnumerable<string> Tags { get; set; } = Enumerable.Empty<string>();

        [MessagePack.Key(2)]
        public IDictionary<string, string> Data { get; set; }
    }
}
