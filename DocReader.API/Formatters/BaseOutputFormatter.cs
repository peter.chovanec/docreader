﻿using DocReader.API.Dtos;
using Microsoft.AspNetCore.Mvc.Formatters;
using System.Text;

namespace DocReader.API.Formatters
{
    public class BaseOutputFormatter : TextOutputFormatter
    {
        private readonly Func<DocDto, string> _format;

        public BaseOutputFormatter(Func<DocDto, string> format)
        {
            _format = format ?? throw new ArgumentNullException(nameof(format));
            SupportedEncodings.Add(Encoding.UTF8);
            SupportedEncodings.Add(Encoding.Unicode);
        }

        public override async Task WriteResponseBodyAsync(OutputFormatterWriteContext context, Encoding selectedEncoding)
        {
            var response = context.HttpContext.Response;

            if (context.Object is DocDto doc)
            {
                await response.WriteAsync(_format(doc), selectedEncoding);
            }
            else if (context.Object is List<DocDto> docList)
            {
                foreach (var docItem in docList)
                {
                    await response.WriteAsync(_format(docItem), selectedEncoding);
                }
            }

        }
    }
}
