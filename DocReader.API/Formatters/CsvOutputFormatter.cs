﻿using DocReader.API.Dtos;
using Microsoft.Net.Http.Headers;
using System.Text;



namespace DocReader.API.Formatters
{
    public class CsvOutputFormatter : BaseOutputFormatter
    {
        public CsvOutputFormatter() : base(Format)
        {
            SupportedMediaTypes.Add(MediaTypeHeaderValue.Parse("application/csv"));
        }

        private static string Format(DocDto doc)
        {
            var buffer = new StringBuilder();

            buffer.AppendLine($"\"{doc.Id}\";\"{String.Join(",", doc.Tags)}\";\"{String.Join(",", doc.Data.Select(x => x.Key + "=" + x.Value))}\"");

            return buffer.ToString().Trim();
        }
    }
}
