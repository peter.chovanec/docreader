﻿using AutoMapper;
using DocReader.API.Dtos;
using DocReader.API.Formatters.Dtos;
using MessagePack;
using Microsoft.Net.Http.Headers;

namespace DocReader.API.Formatters
{
    public class MessagePackOutputFormatter : BaseOutputFormatter
    {
        public MessagePackOutputFormatter() : base(Format)
        {
            SupportedMediaTypes.Add(MediaTypeHeaderValue.Parse("application/x-msgpack"));
        }

        private static string Format(DocDto doc)
        {
            // Automapper can be used for this purpose
            var mpDoc = new MessagePackDocDto
            {
                Id = doc.Id,
                Tags = doc.Tags,
                Data = doc.Data
            };

            var buffer = MessagePackSerializer.Serialize(mpDoc);
            return BitConverter.ToString(buffer);
        }
    }

}
